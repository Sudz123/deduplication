# DEDUPLICATION
This repository contains the code for the De-Duplication of data problem using unsupervised clustering.

DP.csv is the input file.

ASSIGNMENT.py and ASSIGNMENT.ipynb both contain the same code, however the ipythonnotebook can be run block wise to understand changes taking place in small blocks of code.

Output.csv is the output file.

ANALYTICS_PRESENT_PPT will take you through my though process.

Put all the files except Output.csv in the same folder on your PC and run the ASSIGNMENT.py file.
