
# coding: utf-8

# In[634]:


#Necessary imports
import pandas as pd
import numpy as np
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.cluster.hierarchy import fcluster


# In[635]:


#Read the data_frame stored in Dp.csv and print first few rows
df = pd.read_csv('Dp.csv')
#print(df.head())


# In[636]:


#drop from columns 'ln' and 'fn' characters such as Mr, Jr, Sr etc.
df['ln']=df['ln'].str.findall('\w{3,}').str.join(' ')
df['fn']=df['fn'].str.findall('\w{3,}').str.join(' ')
#form a copy of the database for later usage
x=df.copy()


# In[637]:


#row wise, extract features from columns 'ln' and 'fn' 
for index in df.itertuples():
    s=index.ln
    s2=index.fn
    sum=0
    for letter in range(len(s)-1) : 
        sum+=ord(s[letter])-64
    df.at[index.Index,'ln'] = sum 
    sum=0
    for letter in range(len(s2)-1) : 
        sum+=ord(s2[letter])-64
    df.at[index.Index,'fn'] = sum 

#print first few rows of the dataframe
#df.head()



# In[638]:


#convert column 'dob' to Date-Time format and replace the date values by its ordinal value 
df['dob']=pd.to_datetime(df.dob)
df['dob']= df['dob'].apply(lambda x: x.toordinal())

#In case we are willing to drop some user data, then we can use the code below to drop totally
#redundant rows where the values of 'ln','dob',fn' are all simultaneously same. These can be safely
#assumed to be duplicated rows but I was not sure about the sensitivity of the data in hand and hence 
#chose not to drop them
#df1 = df[(df.gn=='F')&~(df.duplicated(subset={'ln','dob','fn'}))]
#df2 = df[(df.gn=='M')&~(df.duplicated(subset={'ln','dob','fn'}))]

#Bifurcate the database into two based on male and female as I assumed people would definitely put
#their genders correct, which I believe is a safe assumption. This gives two child datasets df1 and df2 
#which contain indexed feature sets of each and every row.
df1 = df[(df.gn=='F')]
df2 = df[(df.gn=='M')]
del df1['gn']
del df2['gn']


# In[639]:


#Applying hierarchial clustering using 'ward' method for finding distances between newly formed clusters.
#Although I could use a dendogram to find max_d, I set max_d=1, a pretty low value, since mostly duplicates will
#have almost the same features.
#fcluster method was used to retrieve the clusters, Z is the linkage matrix. The exact documentation 
#can be accessed on Scikit Learn's website.

Z = linkage(df1[['fn','ln','dob']], 'ward')
max_d = 1
clusters = fcluster(Z, max_d, criterion='distance')
Z1 = linkage(df2[['fn','ln','dob']], 'ward')
clusters1 = fcluster(Z1, max_d, criterion='distance')

#Since I want the clusters of the df2 to be labelled differently than clusters of df1, I multiplied clusters1*100.
#One could also use another labelling format

clusters1= clusters1*100


# In[640]:


#Created a column 'Labels' in df1 and df2 which holds the corresponding cluster labels and then
#deleted the other feature values. 
df1['Labels']=clusters
df2['Labels']=clusters1
del df1['fn'],df1['ln'],df1['dob']
del df2['fn'],df2['ln'],df2['dob']

#Concatenated df1 and df2 and merged them with x(the copy I had created), based on index values
pd.merge(x,pd.concat([df1,df2]),how='left',left_index=True,right_index=True).to_csv('example.csv')

